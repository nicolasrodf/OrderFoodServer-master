package com.nicolasrf.orderfoodserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Model.DataMessage;
import com.nicolasrf.orderfoodserver.Model.FCMResponse;
import com.nicolasrf.orderfoodserver.Remote.APIService;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import info.hoang8f.widget.FButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageActivity extends AppCompatActivity {

    MaterialEditText titleEditText, messageEditText;
    FButton sendButton;

    APIService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        titleEditText = findViewById(R.id.title_edit_text);
        messageEditText = findViewById(R.id.message_edit_text);
        sendButton = findViewById(R.id.send_message_button);

        mService = Common.getFCMClient();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = new StringBuilder("TAG ").append(titleEditText.getText()).toString(); //Agrego TAG (puede ser cualquier String) para chequear en OrderApp si es para todos o para ese dispositivo.

                Map<String,String> dataSend = new HashMap<>();
                dataSend.put("title", title );
                dataSend.put("message", messageEditText.getText().toString());
                String to = new StringBuilder("/topics/").append(Common.topicName).toString();
                DataMessage dataMessage = new DataMessage(to, dataSend);

                mService.sendNotification(dataMessage)
                        .enqueue(new Callback<FCMResponse>() {
                            @Override
                            public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                if(response.body().success == 1){
                                    Toast.makeText(SendMessageActivity.this, "Notification send!", Toast.LENGTH_SHORT).show();

                                } else{
                                    Toast.makeText(SendMessageActivity.this, "Failed to send notification.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<FCMResponse> call, Throwable t) {
                                Log.d("TAG", "onFailure: " + t.getMessage());
                            }
                        });

            }
        });
    }
}
