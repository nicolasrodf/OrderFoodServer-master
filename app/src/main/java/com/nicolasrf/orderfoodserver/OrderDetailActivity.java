package com.nicolasrf.orderfoodserver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.ViewHolder.OrderDetailAdapter;

public class OrderDetailActivity extends AppCompatActivity {

    TextView orderIdTextView, orderPhoneTextView, orderAddressTextView, orderTotalTextView, orderCommentTextView;
    String orderIdValue ="";
    RecyclerView foodListRecycler;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        orderIdTextView = findViewById(R.id.shipper_name_text_view);
        orderPhoneTextView = findViewById(R.id.order_phone_text_view);
        orderAddressTextView = findViewById(R.id.order_address_text_view);
        orderTotalTextView = findViewById(R.id.order_total_text_view);
        orderCommentTextView = findViewById(R.id.order_comment_text_view);

        foodListRecycler = findViewById(R.id.food_list_recycler);
        foodListRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        foodListRecycler.setLayoutManager(layoutManager);

        if(getIntent() != null){
            orderIdValue = getIntent().getStringExtra("orderId");
        }

        //Set value
        orderIdTextView.setText(orderIdValue);
        orderPhoneTextView.setText(Common.currentRequest.getPhone());
        orderAddressTextView.setText(Common.currentRequest.getAddress());
        orderTotalTextView.setText(Common.currentRequest.getTotal());
        orderCommentTextView.setText(Common.currentRequest.getComment());

        OrderDetailAdapter adapter = new OrderDetailAdapter(Common.currentRequest.getFoods());
        adapter.notifyDataSetChanged();
        foodListRecycler.setAdapter(adapter);


    }
}
