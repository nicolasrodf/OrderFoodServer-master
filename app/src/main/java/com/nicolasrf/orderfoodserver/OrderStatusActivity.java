package com.nicolasrf.orderfoodserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Model.DataMessage;
import com.nicolasrf.orderfoodserver.Model.FCMResponse;
import com.nicolasrf.orderfoodserver.Model.Request;
import com.nicolasrf.orderfoodserver.Model.Token;
import com.nicolasrf.orderfoodserver.Remote.APIService;
import com.nicolasrf.orderfoodserver.ViewHolder.OrderViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusActivity extends AppCompatActivity {
    private static final String TAG = "OrderStatusActivity";

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<Request,OrderViewHolder> adapter;

    FirebaseDatabase db;
    DatabaseReference requests;

    MaterialSpinner statusSpinner, shipperSpinner;

    APIService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        //Firebase
        db = FirebaseDatabase.getInstance();
        requests = db.getReference("Requests");

        //Init
        recyclerView = findViewById(R.id.order_list_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        mService = Common.getFCMClient();


        loadOrders();

    }

    private void loadOrders() {

        FirebaseRecyclerOptions<Request> options = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(requests,Request.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull OrderViewHolder viewHolder, final int position, @NonNull final Request model) {
                viewHolder.orderIdTextView.setText(adapter.getRef(position).getKey());
                viewHolder.orderStatusTextView.setText(Common.convertCodeToStatus(model.getStatus()));
                viewHolder.orderAddressTextView.setText(model.getAddress());
                viewHolder.orderPhoneTextView.setText(model.getPhone());
                viewHolder.orderDateTextView.setText(Common.getDate(Long.parseLong(adapter.getRef(position).getKey())));//Date a partir del phone key !!!

                viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showUpdateDialog(adapter.getRef(position).getKey(), adapter.getItem(position));
                    }
                });

                viewHolder.removeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteOrder(adapter.getRef(position).getKey());
                    }
                });

                viewHolder.detailButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent orderDetail = new Intent(OrderStatusActivity.this, OrderDetailActivity.class);
                        Common.currentRequest = model;
                        orderDetail.putExtra("orderId", adapter.getRef(position).getKey());
                        startActivity(orderDetail);
                    }
                });

                viewHolder.directionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent trackingOrder = new Intent(OrderStatusActivity.this, TrackingOrderActivity.class);
                        Common.currentRequest = model;
                        startActivity(trackingOrder);
                    }
                });
            }

            @Override
            public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_layout,parent,false);
                return new OrderViewHolder(itemView);
            }
        };
        adapter.startListening();

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

    }

    //crtl + O
    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    //crtkl+O
    @Override
    protected void onResume() {
        super.onResume();
        loadOrders();
        //Fix click back
        if(adapter != null){
            adapter.startListening();
        }
    }

    private void showUpdateDialog(final String key, final Request item) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(OrderStatusActivity.this);
        alertDialog.setTitle("Update Order");
        alertDialog.setMessage("Please choose status");

        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.update_order_layout,null);

        statusSpinner = view.findViewById(R.id.status_spinner);
        statusSpinner.setItems("Placed", "On my way", "Shipping");

        shipperSpinner = view.findViewById(R.id.shipper_spinner);
        //Load all shippers phone to spinner
        final List<String> shipperList = new ArrayList<>();
        FirebaseDatabase.getInstance().getReference("Shippers")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot shipperSnapshot:dataSnapshot.getChildren()){
                            shipperList.add(shipperSnapshot.getKey());
                        }
                        shipperSpinner.setItems(shipperList); //set all shippers phone
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        alertDialog.setView(view);

        //set button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                item.setStatus(String.valueOf(statusSpinner.getSelectedIndex()));

                //
                if(item.getStatus().equals("2")){

                    String shipperPhone = shipperSpinner.getItems().get(shipperSpinner.getSelectedIndex()).toString();

                    //copy item to table "OrderNeedShip"
                    FirebaseDatabase.getInstance().getReference(Common.ORDER_NEED_SHIP_TABLE)
                            .child(shipperPhone)
                            .child(key)
                            .setValue(item);
                    requests.child(key).setValue(item);
                    adapter.notifyDataSetChanged(); //Add to update item size

                    sendToUserNotification(key,item);
                    sendToShipperNotification(shipperPhone,item);

                } else {

                    requests.child(key).setValue(item);
                    adapter.notifyDataSetChanged(); //Add to update item size
                    sendToUserNotification(key, item);
                }
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void sendToShipperNotification(String shipperPhone, Request item) {
        DatabaseReference tokens = db.getReference(Common.token_table);
        tokens.child(shipperPhone)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren()) {
                            Token token = postSnapshot.getValue(Token.class);

                            Map<String,String> dataSend = new HashMap<>();
                            dataSend.put("title", "Nico DEV");
                            dataSend.put("message", "Your have new order ship");
                            DataMessage dataMessage = new DataMessage(token.getToken(), dataSend);

//                            mService.sendNotification(content)
                            mService.sendNotification(dataMessage)
                                    .enqueue(new Callback<FCMResponse>() {
                                        @Override
                                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                            if(response.body().success == 1){
                                                Toast.makeText(OrderStatusActivity.this, "Sent to Shippers!", Toast.LENGTH_SHORT).show();

                                            } else{
                                                Toast.makeText(OrderStatusActivity.this, "Failed to send notification.", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                                            Log.d(TAG, "onFailure: " + t.getMessage());
                                        }
                                    });

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void sendToUserNotification(final String key, Request item) {
        DatabaseReference tokens = db.getReference(Common.token_table);
        tokens.orderByKey().equalTo(item.getPhone())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren()) {
                            Token token = postSnapshot.getValue(Token.class);

                            Map<String,String> dataSend = new HashMap<>();
                            dataSend.put("title", "Nico DEV");
                            dataSend.put("message", "Your order " + key + " was updated");
                            DataMessage dataMessage = new DataMessage(token.getToken(), dataSend);

                            mService.sendNotification(dataMessage)
                                    .enqueue(new Callback<FCMResponse>() {
                                        @Override
                                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                            if(response.body().success == 1){
                                                Toast.makeText(OrderStatusActivity.this, "Order was updated!", Toast.LENGTH_SHORT).show();

                                            } else{
                                                Toast.makeText(OrderStatusActivity.this, "Order was updated but was failed to send notification.", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                                            Log.d(TAG, "onFailure: " + t.getMessage());
                                        }
                                    });

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void deleteOrder(String key) {
        requests.child(key).removeValue();
        adapter.notifyDataSetChanged();
        Toast.makeText(this, "Request deleted !!!", Toast.LENGTH_SHORT).show();
    }
}
