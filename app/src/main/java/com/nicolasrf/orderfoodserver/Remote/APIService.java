package com.nicolasrf.orderfoodserver.Remote;



import com.nicolasrf.orderfoodserver.Model.DataMessage;
import com.nicolasrf.orderfoodserver.Model.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Nicolas on 1/03/2018.
 */

public interface APIService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAiI4IolQ:APA91bGz_ClQrvWfKGr3YE4UrIVgDe9ERgtwV7n-w_mLhzAgJtYcZbWIxOqV6SFOf0oFgVzpUgeAD5y6c8yhG2BOippOBmqpZPjB3wkdqb9iH61aLauarTSOff6EDMfKE3Ermusoto1z"
    })
    @POST("fcm/send")
    Call<FCMResponse> sendNotification(@Body DataMessage body);
}
