package com.nicolasrf.orderfoodserver.Common;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.nicolasrf.orderfoodserver.Model.Request;
import com.nicolasrf.orderfoodserver.Model.User;
import com.nicolasrf.orderfoodserver.Remote.APIService;
import com.nicolasrf.orderfoodserver.Remote.IGeoCoordinates;
import com.nicolasrf.orderfoodserver.Remote.FCMClient;
import com.nicolasrf.orderfoodserver.Remote.RetrofitClient;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Nicolas on 18/03/2018.
 */

public class Common {

    public static final String SHIPPERS_TABLE = "Shippers";
    public static final String ORDER_NEED_SHIP_TABLE = "OrderNeedShip";


    public static User currentUser;
    public static Request currentRequest;
    public static String PHONE_TEXT = "userPhone";

    public static String topicName = "News";

    public static final String UPDATE = "Update";
    public static final String DELETE = "Delete";
    public static final String token_table = "Tokens";

    public static final int PICK_IMAGE_REQUEST = 9999;

    public static final String baseUrl = "https://maps.googleapis.com/";

    public static String convertCodeToStatus(String code){
        if(code.equals("0")){
            return "Placed";
        } else if(code.equals("1")) {
            return "On my way";
        } else if(code.equals("2")){
            return "Shipping";
        } else {
            return "Shipped";
        }
    }

    public static IGeoCoordinates getGeoCodeService() {
        return RetrofitClient.getClient(baseUrl).create(IGeoCoordinates.class);
    }

    /* for FCM service**/
    public static final String fcmUrl = "https://fcm.googleapis.com/";
    public static APIService getFCMClient(){
        return FCMClient.getClient(fcmUrl).create(APIService.class);
    }
    /**/

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight){
        Bitmap scaledBitmap =Bitmap.createBitmap(newWidth,newHeight,Bitmap.Config.ARGB_8888);

        float scaleX = newWidth/(float)bitmap.getWidth();
        float scaleY = newHeight/(float)bitmap.getHeight();
        float pivotX=0, pivotY=0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX,scaleY,pivotX,pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap,0,0,new Paint(Paint.FILTER_BITMAP_FLAG));

        return  scaledBitmap;

    }

    //Date a partir del phone key !!!
    public static String getDate(long time){
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(time);
        StringBuilder date = new StringBuilder(android.text.format.DateFormat.format("dd-MM-yyyy HH:mm",
                calendar).toString());
        return date.toString();
    }

}
