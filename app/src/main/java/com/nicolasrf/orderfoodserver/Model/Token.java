package com.nicolasrf.orderfoodserver.Model;

/**
 * Created by Nicolas on 22/03/2018.
 */

public class Token {

    public String token; //key (userPhone)
    public boolean isServerToken; //tell us is from clien side or server side

    public Token() {
    }

    public Token(String token, boolean isServerToken) {
        this.token = token;
        this.isServerToken = isServerToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isServerToken() {
        return isServerToken;
    }

    public void setServerToken(boolean serverToken) {
        isServerToken = serverToken;
    }
}
