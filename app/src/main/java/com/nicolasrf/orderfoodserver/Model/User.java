package com.nicolasrf.orderfoodserver.Model;

/**
 * Created by Nicolas on 17/03/2018.
 */

public class User {

    private String name, password, phone, isStaff;

    public User() {
    }

    public User(String name, String password, String phone, String isStaff) {
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.isStaff = isStaff;
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getIsStaff() {
        return isStaff;
    }

    public void setIsStaff(String isStaff) {
        this.isStaff = isStaff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
