package com.nicolasrf.orderfoodserver.Model;

/**
 * Created by Nicolas on 18/03/2018.
 */

public class Category {

    private String name, image;

    public Category() {
    }

    public Category(String name, String image) {
        this.name = name;
        this.image = image; //Uri
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
