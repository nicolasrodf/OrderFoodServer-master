package com.nicolasrf.orderfoodserver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Model.User;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "SignInActivity";

    EditText phoneEditText, passwordEditText;
    Button signInButton;

    FirebaseDatabase database;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        passwordEditText = findViewById(R.id.password_edit_text);
        phoneEditText = findViewById(R.id.phone_edit_text);
        signInButton = findViewById(R.id.sign_in_button);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signInUser(phoneEditText.getText().toString(), passwordEditText.getText().toString());
            }
        });
    }

    private void signInUser(String phone, final String password) {

        final ProgressDialog mDialog = new ProgressDialog(SignInActivity.this);
        mDialog.setMessage("Please waiting...");
        mDialog.show();

        final String localPhone = phone;
        final String localPassword = password;

        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Checkl if user not exist in database
                if(dataSnapshot.child(localPhone).exists()) {
                    //Get user
                    mDialog.dismiss();
                    User user = dataSnapshot.child(localPhone).getValue(User.class);
                    user.setPhone(localPhone); //set phone

                    if(Boolean.parseBoolean(user.getIsStaff())) { //if staff == true

                        if (user.getPassword().equals(password)) {

                            Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                            Common.currentUser = user;
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SignInActivity.this, "Wrong password!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignInActivity.this, "Please login with staff account", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    mDialog.dismiss();
                    Toast.makeText(SignInActivity.this, "User not exist in database.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
