package com.nicolasrf.orderfoodserver.Service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Model.Token;

/**
 * Created by Nicolas on 22/03/2018.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if(Common.currentUser != null) {
            updateToken(refreshedToken); //When have refresh tokem we need update to our Realtime database
        }

    }

    private void updateToken(String refreshedToken) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Common.token_table);

        Token token = new Token(refreshedToken,true); //true because this token send from Server app
        tokens.child(Common.currentUser.getPhone()).setValue(token);

    }
}
