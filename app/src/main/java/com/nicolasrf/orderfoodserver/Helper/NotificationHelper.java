package com.nicolasrf.orderfoodserver.Helper;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import com.nicolasrf.orderfoodserver.R;


/**
 * Created by Nicolas on 5/04/2018.
 */

public class NotificationHelper extends ContextWrapper {

    private static final String NICO_CHANNEL_ID = "com.nicolasrf.orderfoodserver.NicoDEV";
    private static final String NICO_CHANNEL_NAME = "Order App";

    private NotificationManager manager;

    public NotificationHelper(Context base) {
        super(base);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//ONly working this fuctioin if API is 26 or higher

            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {

        NotificationChannel nicoChannel = new NotificationChannel(NICO_CHANNEL_ID, NICO_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        nicoChannel.enableLights(false);
        nicoChannel.enableVibration(true);
        nicoChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(nicoChannel);
    }

    public NotificationManager getManager() {

        if(manager == null){
            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getOrderAppChannelNotification(String title, String body, PendingIntent contentIntent, Uri soundUri){

        return new Notification.Builder(getApplicationContext(), NICO_CHANNEL_ID)
                .setContentIntent(contentIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(soundUri)
                .setAutoCancel(false);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getOrderAppChannelNotification(String title, String body, Uri soundUri){

        return new Notification.Builder(getApplicationContext(), NICO_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(soundUri)
                .setAutoCancel(false);
    }
}
