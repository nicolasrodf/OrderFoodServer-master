package com.nicolasrf.orderfoodserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Interface.ItemClickListener;
import com.nicolasrf.orderfoodserver.R;

/**
 * Created by Nicolas on 19/03/2018.
 */

public class BannerViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

    public TextView bannerNameTextView;
    public ImageView bannerImageView;

    private ItemClickListener itemClickListener;


    public BannerViewHolder(View itemView) {
        super(itemView);

        bannerNameTextView = itemView.findViewById(R.id.banner_name_text_view);
        bannerImageView = itemView.findViewById(R.id.banner_image_view);

        itemView.setOnCreateContextMenuListener(this); //crea menu al clickear sobre item.
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.setHeaderTitle("Select the action");
        menu.add(0,0,getAdapterPosition(), Common.UPDATE);
        menu.add(0,1,getAdapterPosition(), Common.DELETE);
    }
}