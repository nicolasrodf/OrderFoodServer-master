package com.nicolasrf.orderfoodserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.Common.Common;
import com.nicolasrf.orderfoodserver.Interface.ItemClickListener;
import com.nicolasrf.orderfoodserver.R;


/**
 * Created by Nicolas on 19/03/2018.
 */

public class FoodViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        View.OnCreateContextMenuListener {

    public TextView foodNameTextView;
    public ImageView foodImageView;

    private ItemClickListener itemClickListener;


    public FoodViewHolder(View itemView) {
        super(itemView);

        foodNameTextView = itemView.findViewById(R.id.food_name_text_view);
        foodImageView = itemView.findViewById(R.id.food_image_view);

        itemView.setOnCreateContextMenuListener(this); //crea menu al clickear sobre item.
        itemView.setOnClickListener(this);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.setHeaderTitle("Select the action");
        menu.add(0,0,getAdapterPosition(), Common.UPDATE);
        menu.add(0,1,getAdapterPosition(), Common.DELETE);
    }
}
