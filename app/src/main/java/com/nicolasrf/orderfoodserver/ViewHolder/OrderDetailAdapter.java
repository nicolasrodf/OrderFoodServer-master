package com.nicolasrf.orderfoodserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.Model.Order;
import com.nicolasrf.orderfoodserver.R;

import java.util.List;

/**
 * Created by Nicolas on 22/03/2018.
 */

class MyViewHolder extends RecyclerView.ViewHolder{

    public TextView nameTextView, quantityTextView, priceTextView, discountTextView;

    public MyViewHolder(View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.product_name_text_view);
        quantityTextView = itemView.findViewById(R.id.product_quantity_text_view);
        priceTextView = itemView.findViewById(R.id.product_price_text_view);
        discountTextView = itemView.findViewById(R.id.product_discount_text_view);
    }
}

public class OrderDetailAdapter extends RecyclerView.Adapter<MyViewHolder>{

    List<Order> myOrders;

    //Constructor generado con Alt+Insert
    public OrderDetailAdapter(List<Order> myOrders) {
        this.myOrders = myOrders;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_layout,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Order order = myOrders.get(position);
        holder.nameTextView.setText(String.format("Name : %s", order.getProductName()));
        holder.quantityTextView.setText(String.format("Quantity : %s", order.getQuantity()));
        holder.priceTextView.setText(String.format("Price : %s", order.getPrice()));
        holder.discountTextView.setText(String.format("Discount : %s", order.getDiscount()));

    }

    @Override
    public int getItemCount() {
        return myOrders.size();
    }
}
