package com.nicolasrf.orderfoodserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.Interface.ItemClickListener;
import com.nicolasrf.orderfoodserver.R;

import info.hoang8f.widget.FButton;

/**
 * Created by Nicolas on 31/03/2018.
 */

public class ShipperViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener{

    public TextView shipperNameTextView, shipperPhoneTextView;
    public FButton editButton, removeButton;

    private ItemClickListener itemClickListener;


    public ShipperViewHolder(View itemView) {
        super(itemView);

        shipperNameTextView = itemView.findViewById(R.id.shipper_name_text_view);
        shipperPhoneTextView = itemView.findViewById(R.id.shipper_phone_text_view);
        editButton = itemView.findViewById(R.id.edit_button);
        removeButton = itemView.findViewById(R.id.remove_button);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);

    }
}
