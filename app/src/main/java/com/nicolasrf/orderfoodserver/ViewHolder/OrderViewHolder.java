package com.nicolasrf.orderfoodserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nicolasrf.orderfoodserver.R;


/**
 * Created by Nicolas on 19/03/2018.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder

{

    public TextView orderIdTextView, orderStatusTextView, orderPhoneTextView, orderAddressTextView, orderDateTextView;
    public Button editButton, removeButton, detailButton, directionButton;


    public OrderViewHolder(View itemView) {
        super(itemView);

        orderIdTextView = itemView.findViewById(R.id.shipper_name_text_view);
        orderDateTextView = itemView.findViewById(R.id.order_date_text_view);
        orderStatusTextView = itemView.findViewById(R.id.order_status_text_view);
        orderPhoneTextView = itemView.findViewById(R.id.order_phone_text_view);
        orderAddressTextView = itemView.findViewById(R.id.order_address_text_view);

        editButton = itemView.findViewById(R.id.edit_button);
        detailButton = itemView.findViewById(R.id.detail_button);
        removeButton = itemView.findViewById(R.id.remove_button);
        directionButton = itemView.findViewById(R.id.direction_button);

    }


}
